package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author Guo Haiqiang
 * @Date 2020/12/2 20:23
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

	@Autowired
	private SupplierService supplierService;

	@RequestMapping("/list")
	public Map<String,Object> list(Integer page, Integer rows, String supplierName) {
		return supplierService.listSupplier(page, rows, supplierName);
	}

	@RequestMapping("/save")
	public void save(Supplier supplier) {
		supplierService.save(supplier);
	}

	@RequestMapping("/delete")
	public void delete(Integer ids) {
		supplierService.delete(ids);
	}


}
