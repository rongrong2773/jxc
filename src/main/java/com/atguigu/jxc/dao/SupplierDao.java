package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Author Guo Haiqiang
 * @Date 2020/12/2 20:42
 */
@Repository
public interface SupplierDao {

	String getMaxCode();

	List<Supplier> getSupplierList(@Param("offSet") Integer offSet,
	                                     @Param("pageRow") Integer pageRow,
	                                     @Param("supplierName") String supplierName);

	int save(@RequestBody Supplier supplier);

	int delete(Integer ids);
}
